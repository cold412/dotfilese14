# musnix-module.nix

{ config, pkgs, lib, ... }:

{
  # Define your musnix configurations here
  # Example configurations:
  environment.systemPackages = with pkgs; [
    # Add packages provided by musnix
   yabridge
   yabridgectl
   wine-wayland 
   reaper
   #x42-avldrums

   #dragonfly-reverb
   #mooSpace
   #distrho
   #lsp-plugins
  ];
   

     environment.variables = let
      makePluginPath = format:
        (lib.strings.makeSearchPath format [
          "$HOME/.nix-profile/lib"
          "/run/current-system/sw/lib"
          "/etc/profiles/per-user/$USER/lib"
        ])
        + ":$HOME/.${format}";
    in {
      DSSI_PATH   = makePluginPath "dssi";
      LADSPA_PATH = makePluginPath "ladspa";
      LV2_PATH    = makePluginPath "lv2";
      LXVST_PATH  = makePluginPath "lxvst";
      VST_PATH    = makePluginPath "vst";
      VST3_PATH   = makePluginPath "vst3";
    };

  # Other musnix-related configurations...

}

